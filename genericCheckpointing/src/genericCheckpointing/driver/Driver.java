package genericCheckpointing.driver;
import java.util.Random;
import java.io.PrintWriter;
import java.io.File;
import genericCheckpointing.util.ProxyCreator;
import genericCheckpointing.server.*;
import genericCheckpointing.util.MyAllTypesFirst;
import genericCheckpointing.util.MyAllTypesSecond;
import genericCheckpointing.util.SerializableObject;
import genericCheckpointing.xmlStoreRestore.StoreRestoreHandler;
// import the other types used in this file

public class Driver {
    
    public static void main(String[] args) {
    	if(!(args[0].equals("serdeser") || args[0].equals("deser"))){
    		System.out.println("Invalid mode");
    		return;
    	}
    	String mode = args[0];
    	int NUM_OF_OBJECTS = Integer.parseInt(args[1]);
    	String outFile = args[2];
		MyAllTypesFirst myFirst;
		MyAllTypesSecond  mySecond;
		ProxyCreator pc = new ProxyCreator();
		StoreRestoreI cpointRef = (StoreRestoreI) pc.createProxy(
								 new Class[] {
								     StoreI.class, RestoreI.class
								 }, 
								 new StoreRestoreHandler()
								 );
		
		if(mode.equals("serdeser")){
		try{
			PrintWriter pw = new PrintWriter(outFile);
			pw.close();
		}catch(Exception e){
			
		}
		Random rand = new Random();
		SerializableObject strs[] = new SerializableObject[NUM_OF_OBJECTS*2];
		for (int i=0; i<2*NUM_OF_OBJECTS; i++) {
		int  n = rand.nextInt(50) + i;
	    myFirst = new MyAllTypesFirst(n,n,n,n,"Design Patterns", true);
	    mySecond = new MyAllTypesSecond(n,n,n,(short)n,(short)n,'a');

	    strs[i] = myFirst;
	    strs[i+1] = mySecond;
	    
	    ((StoreI) cpointRef).writeObj(myFirst, i, "XML", outFile);
	    ((StoreI) cpointRef).writeObj(mySecond, i+1, "XML", outFile);
	    i++;
	}
		 SerializableObject[] myRecordRet = new SerializableObject[NUM_OF_OBJECTS*2];
		 myRecordRet = ((RestoreI) cpointRef).readObj("XML",outFile,NUM_OF_OBJECTS*2);
		 
		 int mismatchCount = 0;
		 for(int i=0;i<2*NUM_OF_OBJECTS;i++){
		 	if(myRecordRet[i] instanceof MyAllTypesSecond){
		 		if(!(strs[i].equals(myRecordRet[i])))
		 			mismatchCount++;
		 	}
		 	else{
		 		if(strs[i].hashCode() != myRecordRet[i].hashCode())
		 			mismatchCount++;
		 	}
		 }
		 System.out.println(mismatchCount + " number of mismatched objects");
	}
	else{
  	 SerializableObject[] myRecordRet = new SerializableObject[10];
	 myRecordRet = ((RestoreI) cpointRef).readObj("XML",outFile,10);
	for(int i=0;i<10;i++){
		 if(myRecordRet[i] instanceof MyAllTypesSecond){
		 	MyAllTypesSecond b = (MyAllTypesSecond)myRecordRet[i];
			System.out.println(b.getMyDoubleT() + ", " + 
							b.getMyOtherDoubleT() + ", " + 
							b.getMyFloatT() + ", " +
							b.getMyShortT() + ", "+
							b.getMyOtherShortT() + ", "+
							b.getMyCharT());
		 }
		 else{
		 	MyAllTypesFirst a = (MyAllTypesFirst)myRecordRet[i];
			System.out.println(a.getMyInt() + ", " + 
							a.getMyOtherInt() + ", " + 
							a.getMyLong() + ", " +
							a.getMyOtherLong() + ", "+
							a.getMyString() + ", "+
							a.getMyBool());
		 }
	   }
	 }
   }
}
