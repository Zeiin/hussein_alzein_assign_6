package genericCheckpointing.xmlStoreRestore;
import java.lang.reflect.*;
import genericCheckpointing.util.*;
public class StoreRestoreHandler implements InvocationHandler{
      public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
           if("writeObj".equals(method.getName())){
               String s;
               Results res = new Results((String)args[3]);
               res.writeLine("<DPSerialization>");
               if(args[0] instanceof MyAllTypesFirst){
                   MyAllTypesFirst a = (MyAllTypesFirst)args[0];
                   res.writeLine(" <complexType xsi:type='genericCheckpointing.util.MyAllTypesFirst'>");
                   if(a.getMyInt() >= 10){
                        s = "  <myInt xsi:type='xsd:int'>" + a.getMyInt() + "</myInt>";
                        res.writeLine(s);
                   }
                   if(a.getMyOtherInt() >= 10){
                        s = "  <myOtherInt xsi:type='xsd:int'>" + a.getMyOtherInt() + "</myOtherInt>";
                        res.writeLine(s);
                   }
                   if(a.getMyLong() >= 10){
                        s = "  <myLong xsi:type='xsd:long'>" + a.getMyLong() + "</myLong>";
                        res.writeLine(s);
                   }
                    if(a.getMyOtherLong() >= 10){
                        s = "  <myOtherLong xsi:type='xsd:long'>" + a.getMyOtherLong() + "</myOtherLong>";
                        res.writeLine(s);
                   }
                        s =  "  <myString xsi:type='xsd:string'>" + a.getMyString() + "</myString>";
                        res.writeLine(s);
                        s =  "  <myBool xsi:type='xsd:boolean'>" + a.getMyBool() + "</myBool>";
                        res.writeLine(s);
                    res.writeLine(" </complexType>");
                    res.writeLine("</DPSerialization>");
                        
               }
               else{
                    MyAllTypesSecond a = (MyAllTypesSecond)args[0];
                   res.writeLine(" <complexType xsi:type='genericCheckpointing.util.MyAllTypesSecond'>");
                   if(a.getMyDoubleT() >= 10){
                        s = "  <myDoubleT xsi:type='xsd:double'>" + a.getMyDoubleT() + "</myDoubleT>";
                        res.writeLine(s);
                   }
                   if(a.getMyOtherDoubleT() >= 10){
                        s = "  <myOtherDoubleT xsi:type='xsd:double'>" + a.getMyOtherDoubleT() + "</myOtherDoubleT>";
                        res.writeLine(s);
                   }
                        s = "  <myFloatT xsi:type='xsd:float'>" + a.getMyFloatT() + "</myFloatT>";
                        res.writeLine(s);
                        
                        s = "  <myShortT xsi:type='xsd:short'>" + a.getMyShortT() + "</myShortT>";
                        res.writeLine(s);
                        
                        s =  "  <myOtherShortT xsi:type='xsd:short'>" + a.getMyOtherShortT() + "</myOtherShortT>";
                        res.writeLine(s);
                        
                        s =  "  <myCharT xsi:type='xsd:char'>" + a.getMyCharT() + "</myCharT>";
                        res.writeLine(s);
                    res.writeLine(" </complexType>");
                    res.writeLine("</DPSerialization>");
               }
           }
            else if("readObj".equals(method.getName())){
               String s;
               SerializableObject[] arr = new SerializableObject[(int)args[2]];
               FileProcessor inp = new FileProcessor((String)args[1]);
               MyAllTypesFirst a =  new MyAllTypesFirst();
               MyAllTypesSecond b = new MyAllTypesSecond();
               boolean usingFirst = false;
                    String line = inp.readLine();
                    int i=0;
                    while(line != null && i<((int)args[2])) {
                         if(line.contains("MyAllTypesFirst")){
                            usingFirst = true;
                             while(!(line.contains("</DPSerialization>"))){
                                  line = inp.readLine();
                                  if(line.contains("myInt")){
                                      int idxa = line.indexOf(">");
                                      int idxb = line.lastIndexOf("<");
                                      String subr = line.substring(idxa+1, idxb);
                                      a.setMyInt(Integer.parseInt(subr));
                                  }
                                  else if(line.contains("myOtherInt")){
                                      int idxa = line.indexOf(">");
                                      int idxb = line.lastIndexOf("<");
                                      String subr = line.substring(idxa+1, idxb);
                                      a.setMyOtherInt(Integer.parseInt(subr));
                                  }
                                  else if(line.contains("myLong")){
                                      int idxa = line.indexOf(">");
                                      int idxb = line.lastIndexOf("<");
                                      String subr = line.substring(idxa+1, idxb);
                                      a.setMyLong(Long.parseLong(subr));
                                  }
                                  else if(line.contains("myOtherLong")){
                                      int idxa = line.indexOf(">");
                                      int idxb = line.lastIndexOf("<");
                                      String subr = line.substring(idxa+1, idxb);
                                      a.setMyOtherLong(Long.parseLong(subr));
                                  }
                                  else if(line.contains("myString")){
                                      int idxa = line.indexOf(">");
                                      int idxb = line.lastIndexOf("<");
                                      String subr = line.substring(idxa+1, idxb);
                                      a.setMyString(subr);
                                  }
                                  else if(line.contains("myBool")){
                                      if(line.contains("true"))
                                        a.setMyBool(true);
                                      else
                                        a.setMyBool(false);
                                  }

                             }
                         }
                         else if(line.contains("MyAllTypesSecond")){
                               usingFirst = false;
                             while(!(line.contains("</DPSerialization>"))){
                                  line = inp.readLine();
                                  if(line.contains("myDoubleT")){
                                      int idxa = line.indexOf(">");
                                      int idxb = line.lastIndexOf("<");
                                      String subr = line.substring(idxa+1, idxb);
                                      b.setMyDoubleT(Double.parseDouble(subr));
                                  }
                                  else if(line.contains("myOtherDoubleT")){
                                      int idxa = line.indexOf(">");
                                      int idxb = line.lastIndexOf("<");
                                      String subr = line.substring(idxa+1, idxb);
                                      b.setMyOtherDoubleT(Double.parseDouble(subr));
                                  }
                                  else if(line.contains("myFloatT")){
                                      int idxa = line.indexOf(">");
                                      int idxb = line.lastIndexOf("<");
                                      String subr = line.substring(idxa+1, idxb);
                                      b.setMyFloatT(Float.parseFloat(subr));
                                  }
                                  else if(line.contains("myShortT")){
                                      int idxa = line.indexOf(">");
                                      int idxb = line.lastIndexOf("<");
                                      String subr = line.substring(idxa+1, idxb);
                                      b.setMyShortT(Short.parseShort(subr));
                                  }
                                  else if(line.contains("myOtherShortT")){
                                      int idxa = line.indexOf(">");
                                      int idxb = line.lastIndexOf("<");
                                      String subr = line.substring(idxa+1, idxb);
                                      b.setMyOtherShortT(Short.parseShort(subr));
                                  }
                                  else if(line.contains("myCharT")){
                                      int idxa = line.indexOf(">");
                                      int idxb = line.lastIndexOf("<");
                                      b.setMyCharT(line.charAt(idxa+1));
                                  }

                             }
                         }
                        if(line.contains("</DPSerialization>")){
                            if(usingFirst){
                                arr[i] = a;
                                a = new MyAllTypesFirst();
                            }
                            else{
                                arr[i] = b;
                                b = new MyAllTypesSecond();
                            }
                            i++;
                        }
                        line = inp.readLine();
                    }
                    return arr;
            }
           return null;
       }
    
}