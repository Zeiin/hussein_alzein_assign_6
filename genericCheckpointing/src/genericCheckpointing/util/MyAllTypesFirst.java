package genericCheckpointing.util;
import genericCheckpointing.util.SerializableObject;
public class MyAllTypesFirst extends SerializableObject
{
    int myInt; 
    int myOtherInt;
    long myLong;
    long myOtherLong; 
    String myString; 
    boolean myBool; 
   
    
        public MyAllTypesFirst(){
            myInt = 0;
            myOtherInt = 0;
            myLong = 0;
            myOtherLong = 0; 
            myString = "";
            myBool = false;
        }
        public MyAllTypesFirst(int a, int b, long c, long d, String e, boolean f){
            myInt = a;
            myOtherInt = b;
            myLong = c;
            myOtherLong = d; 
            myString = e;
            myBool = f;
        }
        @Override
        public boolean equals(Object other) {
            if (other == this) return true;
            if (!(other instanceof MyAllTypesFirst)) {
                    return false;
                }
             MyAllTypesFirst b = (MyAllTypesFirst)other;
    
            return  (b.myInt == myInt &&
                    b.myOtherInt == myOtherInt &&
                    b.myLong == myLong &&
                    b.myOtherLong == myOtherLong &&
                    b.myString.equals(myString) &&
                    b.myBool == myBool);
        }
        @Override
        public int hashCode() {
            int result = 17;
            result = 31 * result + myInt;
            result = 31 * result + myOtherInt;
            result = 31 * result + (int)myLong;
            result = 31 * result + (int)myOtherLong;
            result = 31 * result + myString.hashCode();
            if(myBool)
            result = 31 * result;
            return result;
        }
        public int getMyInt(){
            return myInt;
        }
        public void setMyInt(int n){
            myInt = n;
        }
        public int getMyOtherInt(){
            return myOtherInt;
        }
        public void setMyOtherInt(int n){
            myOtherInt = n;
        }
       public long getMyLong(){
            return myLong;
        }
        public void setMyLong(long n){
            myLong = n;
        }
        public long getMyOtherLong(){
            return myOtherLong;
        }
        public void setMyOtherLong(long n){
            myOtherLong = n;
        }
        
        public String getMyString(){
            return myString;
        }
        public void setMyString(String n){
            myString = n;
        }
        
        public boolean getMyBool(){
            return myBool;
        }
        public void setMyBool(boolean n){
            myBool = n;
        }
}