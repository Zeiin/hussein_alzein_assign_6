package genericCheckpointing.util;
import genericCheckpointing.util.SerializableObject;
public class MyAllTypesSecond extends SerializableObject
{
    double myDoubleT; 
    double myOtherDoubleT; 
    float myFloatT; 
    short myShortT; 
    short myOtherShortT; 
    char myCharT; 
    
        public MyAllTypesSecond(){
            myDoubleT = 0;
            myOtherDoubleT = 0; 
            myFloatT = 0;
            myShortT = 0;
            myOtherShortT = 0; 
            myCharT  = 0;
        }
        public MyAllTypesSecond(double a, double b, float c, short d, short e, char f){
            myDoubleT = a;
            myOtherDoubleT = b; 
            myFloatT = c;
            myShortT = d;
            myOtherShortT = e; 
            myCharT  = f;
        }
        @Override
        public boolean equals(Object other) {
            if (other == this) return true;
            if (!(other instanceof MyAllTypesSecond)) {
                    return false;
                }
             MyAllTypesSecond b = (MyAllTypesSecond)other;
    
            return  (b.myDoubleT == myDoubleT &&
                    b.myOtherDoubleT == myOtherDoubleT &&
                    b.myFloatT == myFloatT &&
                    b.myShortT == myShortT &&
                    b.myOtherShortT == myOtherShortT &&
                    b.myCharT == myCharT);
        }
        @Override
        public int hashCode() {
            double result = 17;
            result = 31 * result + myDoubleT;
            result = 31 * result + myOtherDoubleT;
            result = 31 * result + myFloatT;
            result = 31 * result + myShortT;
            result = 31 * result + myOtherShortT;
            result = 31 * result + myCharT;
            return (int)result;
        }
         public double getMyDoubleT(){
            return myDoubleT;
        }
        public void setMyDoubleT(double n){
            myDoubleT = n;
        }
        public double getMyOtherDoubleT(){
            return myOtherDoubleT;
        }
        public void setMyOtherDoubleT(double n){
            myOtherDoubleT = n;
        }
         public short getMyShortT(){
            return myShortT;
        }
        public void setMyShortT(short n){
            myShortT = n;
        }
        public short getMyOtherShortT(){
            return myOtherShortT;
        }
        public void setMyOtherShortT(short n){
            myOtherShortT = n;
        }
        public float getMyFloatT(){
            return myFloatT;
        }
        public void setMyFloatT(float n){
            myFloatT = n;
        }
        public char getMyCharT(){
            return myCharT;
        }
        public void setMyCharT(char n){
            myCharT = n;
        }
}
