<DPSerialization>
 <complexType xsi:type="genericCheckpointing.util.MyAllTypesFirst">
  <myInt xsi:type="xsd:int">314</myInt>
  <myLong xsi:type="xsd:long">314159</myLong>
  <myString xsi:type="xsd:string">Design Patterns</myString>
  <myBool xsi:type="xsd:boolean">false</myBool>
  <myOtherInt xsi:type="xsd:int">314</myOtherInt>
 </complexType>
</DPSerialization>
<DPSerialization>
 <complexType xsi:type="genericCheckpointing.util.MyAllTypesSecond">
  <myDoubleT xsi:type="xsd:double">3.1459</myDoubleT>
  <myFloatT xsi:type="xsd:float">3145.9</myFloatT>
  <myShortT xsi:type="xsd:short">314</myShortT>
  <myCharT xsi:type="xsd:char">P</myCharT>
 </complexType>
</DPSerialization>
<DPSerialization>
 <complexType xsi:type="genericCheckpointing.util.MyAllTypesSecond">
  <myFloatT xsi:type="xsd:float">314.59</myFloatT>
  <myShortT xsi:type="xsd:short">31</myShortT>
  <myCharT xsi:type="xsd:char">Q</myCharT>
  <myDoubleT xsi:type="xsd:double">31.459</myDoubleT>
 </complexType>
</DPSerialization>
<DPSerialization>
 <complexType xsi:type="genericCheckpointing.util.MyAllTypesSecond">
  <myFloatT xsi:type="xsd:float">31.459</myFloatT>
  <myShortT xsi:type="xsd:short">31</myShortT>
  <myDoubleT xsi:type="xsd:double">314.59</myDoubleT>
  <myOtherDoubleT xsi:type="xsd:double">314.59</myOtherDoubleT>
  <myCharT xsi:type="xsd:char">R</myCharT>
 </complexType>
</DPSerialization>
<DPSerialization>
 <complexType xsi:type="genericCheckpointing.util.MyAllTypesSecond">
  <myShortT xsi:type="xsd:short">3</myShortT>
  <myCharT xsi:type="xsd:char">R</myCharT>
  <myDoubleT xsi:type="xsd:double">3145.9</myDoubleT>
  <myFloatT xsi:type="xsd:float">3.1459</myFloatT>
  <myOtherDoubleT xsi:type="xsd:double">314.59</myOtherDoubleT>
 </complexType>
</DPSerialization>
<DPSerialization>
 <complexType xsi:type="genericCheckpointing.util.MyAllTypesFirst">
  <myLong xsi:type="xsd:long">951413</myLong>
  <myString xsi:type="xsd:string">2-Design Patterns</myString>
  <myInt xsi:type="xsd:int">413</myInt>
  <myOtherLong xsi:type="xsd:long">951413</myOtherLong>
  <myBool xsi:type="xsd:boolean">true</myBool>
 </complexType>
</DPSerialization>
<DPSerialization>
 <complexType xsi:type="genericCheckpointing.util.MyAllTypesFirst">
  <myString xsi:type="xsd:string">3-Design Patterns</myString>
  <myBool xsi:type="xsd:boolean">true</myBool>
  <myInt xsi:type="xsd:int">413314</myInt>
  <myLong xsi:type="xsd:long">9514</myLong>
 </complexType>
</DPSerialization>
<DPSerialization>
 <complexType xsi:type='genericCheckpointing.util.MyAllTypesFirst'>
  <myInt xsi:type='xsd:int'>35</myInt>
  <myOtherInt xsi:type='xsd:int'>35</myOtherInt>
  <myLong xsi:type='xsd:long'>35</myLong>
  <myOtherLong xsi:type='xsd:long'>35</myOtherLong>
  <myString xsi:type='xsd:string'>Design Patterns</myString>
  <myBool xsi:type='xsd:boolean'>true</myBool>
 </complexType>
</DPSerialization>
<DPSerialization>
 <complexType xsi:type='genericCheckpointing.util.MyAllTypesSecond'>
  <myDoubleT xsi:type='xsd:double'>35.0</myDoubleT>
  <myOtherDoubleT xsi:type='xsd:double'>35.0</myOtherDoubleT>
  <myFloatT xsi:type='xsd:float'>35.0</myFloatT>
  <myShortT xsi:type='xsd:short'>35</myShortT>
  <myOtherShortT xsi:type='xsd:short'>35</myOtherShortT>
  <myCharT xsi:type='xsd:char'>a</myCharT>
 </complexType>
</DPSerialization>
<DPSerialization>
 <complexType xsi:type='genericCheckpointing.util.MyAllTypesFirst'>
  <myInt xsi:type='xsd:int'>53</myInt>
  <myOtherInt xsi:type='xsd:int'>53</myOtherInt>
  <myLong xsi:type='xsd:long'>53</myLong>
  <myOtherLong xsi:type='xsd:long'>53</myOtherLong>
  <myString xsi:type='xsd:string'>Design Patterns</myString>
  <myBool xsi:type='xsd:boolean'>true</myBool>
 </complexType>
</DPSerialization>
<DPSerialization>
 <complexType xsi:type='genericCheckpointing.util.MyAllTypesSecond'>
  <myDoubleT xsi:type='xsd:double'>53.0</myDoubleT>
  <myOtherDoubleT xsi:type='xsd:double'>53.0</myOtherDoubleT>
  <myFloatT xsi:type='xsd:float'>53.0</myFloatT>
  <myShortT xsi:type='xsd:short'>53</myShortT>
  <myOtherShortT xsi:type='xsd:short'>53</myOtherShortT>
  <myCharT xsi:type='xsd:char'>a</myCharT>
 </complexType>
</DPSerialization>
<DPSerialization>
 <complexType xsi:type='genericCheckpointing.util.MyAllTypesFirst'>
  <myInt xsi:type='xsd:int'>36</myInt>
  <myOtherInt xsi:type='xsd:int'>36</myOtherInt>
  <myLong xsi:type='xsd:long'>36</myLong>
  <myOtherLong xsi:type='xsd:long'>36</myOtherLong>
  <myString xsi:type='xsd:string'>Design Patterns</myString>
  <myBool xsi:type='xsd:boolean'>true</myBool>
 </complexType>
</DPSerialization>
<DPSerialization>
 <complexType xsi:type='genericCheckpointing.util.MyAllTypesSecond'>
  <myDoubleT xsi:type='xsd:double'>36.0</myDoubleT>
  <myOtherDoubleT xsi:type='xsd:double'>36.0</myOtherDoubleT>
  <myFloatT xsi:type='xsd:float'>36.0</myFloatT>
  <myShortT xsi:type='xsd:short'>36</myShortT>
  <myOtherShortT xsi:type='xsd:short'>36</myOtherShortT>
  <myCharT xsi:type='xsd:char'>a</myCharT>
 </complexType>
</DPSerialization>
<DPSerialization>
 <complexType xsi:type='genericCheckpointing.util.MyAllTypesFirst'>
  <myInt xsi:type='xsd:int'>27</myInt>
  <myOtherInt xsi:type='xsd:int'>27</myOtherInt>
  <myLong xsi:type='xsd:long'>27</myLong>
  <myOtherLong xsi:type='xsd:long'>27</myOtherLong>
  <myString xsi:type='xsd:string'>Design Patterns</myString>
  <myBool xsi:type='xsd:boolean'>true</myBool>
 </complexType>
</DPSerialization>
<DPSerialization>
 <complexType xsi:type='genericCheckpointing.util.MyAllTypesSecond'>
  <myDoubleT xsi:type='xsd:double'>27.0</myDoubleT>
  <myOtherDoubleT xsi:type='xsd:double'>27.0</myOtherDoubleT>
  <myFloatT xsi:type='xsd:float'>27.0</myFloatT>
  <myShortT xsi:type='xsd:short'>27</myShortT>
  <myOtherShortT xsi:type='xsd:short'>27</myOtherShortT>
  <myCharT xsi:type='xsd:char'>a</myCharT>
 </complexType>
</DPSerialization>
<DPSerialization>
 <complexType xsi:type='genericCheckpointing.util.MyAllTypesFirst'>
  <myInt xsi:type='xsd:int'>38</myInt>
  <myOtherInt xsi:type='xsd:int'>38</myOtherInt>
  <myLong xsi:type='xsd:long'>38</myLong>
  <myOtherLong xsi:type='xsd:long'>38</myOtherLong>
  <myString xsi:type='xsd:string'>Design Patterns</myString>
  <myBool xsi:type='xsd:boolean'>true</myBool>
 </complexType>
</DPSerialization>
<DPSerialization>
 <complexType xsi:type='genericCheckpointing.util.MyAllTypesSecond'>
  <myDoubleT xsi:type='xsd:double'>38.0</myDoubleT>
  <myOtherDoubleT xsi:type='xsd:double'>38.0</myOtherDoubleT>
  <myFloatT xsi:type='xsd:float'>38.0</myFloatT>
  <myShortT xsi:type='xsd:short'>38</myShortT>
  <myOtherShortT xsi:type='xsd:short'>38</myOtherShortT>
  <myCharT xsi:type='xsd:char'>a</myCharT>
 </complexType>
</DPSerialization>
